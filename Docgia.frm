VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form Form5 
   Caption         =   "Form5"
   ClientHeight    =   7200
   ClientLeft      =   2985
   ClientTop       =   1980
   ClientWidth     =   12075
   LinkTopic       =   "Form5"
   ScaleHeight     =   7200
   ScaleWidth      =   12075
   Begin MSDataGridLib.DataGrid db2 
      Height          =   5655
      Left            =   5400
      TabIndex        =   12
      Top             =   1200
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   9975
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   25
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   ".VnTime"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   ".VnTime"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdThem 
      Caption         =   "Th�m"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   960
      TabIndex        =   7
      Top             =   4200
      Width           =   1215
   End
   Begin VB.TextBox txtmadocgia 
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      TabIndex        =   6
      Top             =   1320
      Width           =   2895
   End
   Begin VB.TextBox txthoten 
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      TabIndex        =   5
      Top             =   2160
      Width           =   2895
   End
   Begin VB.TextBox txtngaysinh 
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      TabIndex        =   4
      Top             =   3000
      Width           =   2895
   End
   Begin VB.CommandButton cmdThoat 
      Caption         =   "Tho�t"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2880
      TabIndex        =   3
      Top             =   5280
      Width           =   1215
   End
   Begin VB.CommandButton cmdXoa 
      Caption         =   "X�a"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2880
      TabIndex        =   2
      Top             =   4200
      Width           =   1215
   End
   Begin VB.CommandButton cmdsua 
      Caption         =   "S�a"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   960
      TabIndex        =   1
      Top             =   5280
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "In Th� Th� Vi�n"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1200
      TabIndex        =   0
      Top             =   6240
      Width           =   2535
   End
   Begin VB.Label Label1 
      BackColor       =   &H80000002&
      Caption         =   "                                        ��c Gi�"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   1
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   12495
   End
   Begin VB.Label lblmadocgia 
      BackStyle       =   0  'Transparent
      Caption         =   "M� ��c Gi�:"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   1
      Left            =   120
      TabIndex        =   15
      Top             =   1320
      Width           =   1935
   End
   Begin VB.Label lblhoten 
      BackStyle       =   0  'Transparent
      Caption         =   "H� T�n:"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   2160
      Width           =   1455
   End
   Begin VB.Label lblngaysinh 
      BackStyle       =   0  'Transparent
      Caption         =   "Ng�y Sinh:"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   120
      TabIndex        =   13
      Top             =   3000
      Width           =   1695
   End
   Begin VB.Image Image1 
      Height          =   18000
      Left            =   0
      Picture         =   "Docgia.frx":0000
      Top             =   0
      Width           =   28800
   End
   Begin VB.Label Label1 
      BackColor       =   &H80000002&
      Caption         =   "                                        ��c Gi�"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   0
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   12135
   End
   Begin VB.Label lblmadocgia 
      BackStyle       =   0  'Transparent
      Caption         =   "M� ��c Gi�"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   360
      TabIndex        =   10
      Top             =   1680
      Width           =   1695
   End
   Begin VB.Label lblhoten 
      BackStyle       =   0  'Transparent
      Caption         =   "H� T�n"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   1
      Left            =   360
      TabIndex        =   9
      Top             =   2520
      Width           =   1455
   End
   Begin VB.Label lblngaysinh 
      BackStyle       =   0  'Transparent
      Caption         =   "Ng�y Sinh"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   2
      Left            =   360
      TabIndex        =   8
      Top             =   3360
      Width           =   1455
   End
End
Attribute VB_Name = "Form5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Con As New ADODB.Connection
Dim Rec As New ADODB.Recordset

Private Sub cmdThoat_Click()
Form5.Hide
Form3.Show
End Sub

Private Sub Command1_Click()
Form6.Show
Form5.Hide
End Sub

Private Sub db2_Click()
db2.Col = 0
txtmadocgia.Text = db2.Text
a = txtmadocgia.Text
db2.Col = 1
txthoten.Text = db2.Text
a1 = txthoten.Text
db2.Col = 2
txtngaysinh.Text = db2.Text
a2 = txtngaysinh.Text
End Sub

Private Sub Form_Load()
 Con.ConnectionString = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Qlthuvien;Data Source=THIENBAO\SQLEXPRESS"
 Con.Open
 Con.CursorLocation = adUseClient
 Rec.Open "Docgia", Con
 Set db2.DataSource = Rec
End Sub
 Private Sub loaddulieu()
Dim Rec As New ADODB.Recordset
Dim str
str = "select * from Docgia"
Rec.Open str, Con
Set db2.DataSource = Rec
End Sub
Private Sub cmdThem_Click()
Dim Rec As ADODB.Recordset
Dim strsql As String
If txtmadocgia.Text = "" Or txthoten.Text = "" Or txtngaysinh.Text = "" Then
MsgBox "Ban phai nhap du thong tin can them"
Else

strsql = "insert into Docgia (Madocgia, Hoten, Ngaysinh) values (" _
& "'" & txtmadocgia.Text & "'," _
& "'" & txthoten.Text & "'," _
& "'" & txtngaysinh.Text & "')"
Set Rec = Con.Execute(strsql)
MsgBox "Luu thanh cong"
End If
loaddulieu
moi
End Sub
Private Sub cmdXoa_Click()
Dim Rec As ADODB.Recordset
Dim strsql As String
If txtmadocgia.Text = "" Or txthoten.Text = "" Or txtngaysinh.Text = "" Then
MsgBox "Ban phai chon phan can xoa"
Else
strsql = "delete from Docgia where Madocgia = '" & txtmadocgia.Text & "'"
Set Rec = Con.Execute(strsql)
MsgBox "Xoa thanh cong"
End If
loaddulieu
moi
End Sub
Private Sub cmdsua_Click()
Dim Rec As New ADODB.Recordset
Dim str As String
If txtmadocgia.Text = "" Or txthoten.Text = "" Or txtngaysinh.Text = "" Then
MsgBox "Ban phai nhap du thong tin can sua"
Else
str = "select * from Docgia where Madocgia='" & Trim(txtmadocgia.Text) & "'"
Rec.Open str, Con
str = " update Docgia set Hoten='" & Trim(txthoten.Text) & "', Ngaysinh='" & Trim(txtngaysinh.Text) & "' where Madocgia='" & Trim(txtmadocgia.Text) & "'"
Con.Execute (str)
MsgBox "Sua thanh cong "
End If
loaddulieu
moi
End Sub
Private Sub moi()
txtmadocgia.Text = ""
txthoten.Text = ""
txtngaysinh.Text = ""
End Sub


