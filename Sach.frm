VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form Form4 
   Caption         =   "Form4"
   ClientHeight    =   6660
   ClientLeft      =   3870
   ClientTop       =   2415
   ClientWidth     =   11745
   BeginProperty Font 
      Name            =   ".VnTime"
      Size            =   14.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form4"
   ScaleHeight     =   6660
   ScaleWidth      =   11745
   Begin VB.CommandButton cmdThem 
      Caption         =   "Th�m"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   720
      TabIndex        =   8
      Top             =   5760
      Width           =   1335
   End
   Begin VB.TextBox txttensach 
      Height          =   495
      Left            =   2400
      TabIndex        =   7
      Top             =   5040
      Width           =   3135
   End
   Begin VB.TextBox txtmasach 
      Height          =   495
      Left            =   2400
      TabIndex        =   6
      Top             =   4080
      Width           =   3135
   End
   Begin VB.CommandButton cmdXoa 
      Caption         =   "X�a"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4680
      TabIndex        =   5
      Top             =   5760
      Width           =   1335
   End
   Begin VB.CommandButton cmdsua 
      Caption         =   "S�a"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2760
      TabIndex        =   4
      Top             =   5760
      Width           =   1215
   End
   Begin VB.TextBox txttacgia 
      Height          =   495
      Left            =   8040
      TabIndex        =   3
      Top             =   5040
      Width           =   3135
   End
   Begin VB.CommandButton cmdThoat 
      Caption         =   "Tho�t"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6840
      TabIndex        =   2
      Top             =   5760
      Width           =   1335
   End
   Begin VB.ComboBox Combo1 
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      ItemData        =   "Sach.frx":0000
      Left            =   8040
      List            =   "Sach.frx":0019
      TabIndex        =   1
      Top             =   4080
      Width           =   3150
   End
   Begin MSDataGridLib.DataGrid db1 
      Height          =   2895
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   5106
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   25
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   ".VnTime"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackColor       =   &H80000003&
      Caption         =   "                                       S�ch"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   27.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   0
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   12255
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "T�n NXB:"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Left            =   6240
      TabIndex        =   12
      Top             =   4080
      Width           =   1575
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "T�n S�ch:"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Left            =   600
      TabIndex        =   11
      Top             =   5040
      Width           =   1575
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "M� S�ch:"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Index           =   1
      Left            =   600
      TabIndex        =   10
      Top             =   4080
      Width           =   1575
   End
   Begin VB.Label lbltacgia 
      BackStyle       =   0  'Transparent
      Caption         =   "T�c Gi�:"
      BeginProperty Font 
         Name            =   ".VnTime"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Left            =   6240
      TabIndex        =   9
      Top             =   5040
      Width           =   1575
   End
   Begin VB.Image Image2 
      Height          =   7050
      Left            =   0
      Picture         =   "Sach.frx":00B8
      Top             =   0
      Width           =   11700
   End
End
Attribute VB_Name = "Form4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Con As New ADODB.Connection
Dim Rec As New ADODB.Recordset
Private Sub Form_Load()
 Con.ConnectionString = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Qlthuvien;Data Source=THIENBAO\SQLEXPRESS"
 Con.Open
 Con.CursorLocation = adUseClient
 Rec.Open "Sach", Con
 Set db1.DataSource = Rec
End Sub

Private Sub cmdsua_Click()
Dim Rec As New ADODB.Recordset
Dim str As String
If txtmasach.Text = "" Or txttensach.Text = "" Then
MsgBox "Ban phai nhap du thong tin can sua"
Else
str = "select * from Sach where Masach='" & Trim(txtmasach.Text) & "'"
Rec.Open str, Con
str = " update Sach set Tensach='" & Trim(txttensach.Text) & "', TenNXB='" & Trim(Combo1.Text) & "' where Masach='" & Trim(txtmasach.Text) & "'"
Con.Execute (str)
MsgBox "Sua thanh cong "
End If
loaddulieu
moi
End Sub

Private Sub loaddulieu()
Dim Rec As New ADODB.Recordset
Dim str, i
str = "select * from Sach"
Rec.Open str, Con
Set db1.DataSource = Rec

End Sub

Private Sub Command1_Click()
Form4.Hide
Form7.Show
End Sub


Private Sub db1_Click()
db1.Col = 0
txtmasach.Text = db1.Text
db1.Col = 1
txttensach.Text = db1.Text
db1.Col = 2
Combo1.Text = db1.Text
db1.Col = 3
txttacgia.Text = db1.Text
End Sub

Private Sub cmdThoat_Click()
Form4.Hide
Form3.Show
End Sub
'xoa can dien du thong tin
Private Sub cmdXoa_Click()
Dim Rec As ADODB.Recordset
Dim strsql As String
If txtmasach.Text = "" Or txttensach.Text = "" Then
MsgBox "Ban phai chon phan can xoa"
Else
strsql = "Xoa '" & txtmasach.Text & "'"
Set Rec = Con.Execute(strsql)
MsgBox "Xoa thanh cong"
End If
loaddulieu
moi
End Sub

Private Sub cmdThem_Click(Index As Integer)
Dim Rec As ADODB.Recordset
Dim strsql As String
If txtmasach.Text = "" Or txttensach.Text = "" Or txttacgia.Text = "" Then
MsgBox "Ban phai nhap du thong tin can them"
Else
strsql = "insert into Sach (Masach, Tensach, TenNXB, Tacgia) values (" _
& "'" & txtmasach.Text & "'," _
& "'" & txttensach.Text & "'," _
& "'" & Combo1.Text & "'," _
& "'" & txttacgia.Text & "')"
Set Rec = Con.Execute(strsql)
MsgBox "Them thanh cong"
End If
loaddulieu
moi
End Sub

Private Sub moi()
txtmasach.Text = ""
txttensach.Text = ""
txttacgia.Text = ""
Combo1.Text = ""

End Sub



